import { Component } from '@angular/core';
import { AuthService } from '../user/auth.service';

@Component({
  templateUrl: './welcome.component.html'
})
export class WelcomeComponent {
  public pageTitle = 'Welcome';

  constructor(public authService: AuthService) { }


}
