import { TestBed } from '@angular/core/testing';
import { Http } from '@angular/http';
import { MessageService } from '../messages/message.service';
import { AuthService } from './auth.service';
describe('AuthService', () => {
  let service: AuthService;
  beforeEach(() => {
    const httpStub = { post: () => ({ subscribe: () => ({}) }) };
    const messageServiceStub = { addMessage: () => ({}) };
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Http, useValue: httpStub },
        { provide: MessageService, useValue: messageServiceStub }
      ]
    });
    service = TestBed.get(AuthService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
