import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { User } from './user';
import { MessageService } from '../messages/message.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: User;
  redirectUrl: string;

  get isLoggedIn(): boolean {
    return !!this.currentUser;
  }

  constructor(private http: Http, private messageService: MessageService) { }

  login(loginData: any): void {
    const userName = loginData.userName;
    const password = loginData.password;
    if (!userName || !password) {
      this.messageService.addMessage('Please enter your userName and password');
      return;
    }

    this.http.post('http://www.mocky.io/v2/5c5160eb340000fc25129c3e', loginData).subscribe(res => {
      localStorage.setItem('token', res.json().jwt);
      const serviceResponse = res.json();
      if (serviceResponse != null) {
        this.currentUser = serviceResponse;
        if (userName === 'admin') {
          this.messageService.addMessage('Admin login');
          return;
        }
        console.log(this.currentUser);
        this.messageService.addMessage(`User: ${userName} logged in`);
        return;
      }
    }, error => {
      this.messageService.addMessage(`Some error occured, please login after some time`);
      return;
    });
  }

  logout(): void {
    this.currentUser = null;
  }
}
