import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { LoginComponent } from './login.component';
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  const formValues = { userName: 'aa', password: 'bb' };
  const ngFormStub = {
    valid: {},
    form: { value: { userName: 'aa', password: 'bb' } },
    submitted: true
  };
  beforeEach(() => {
    const routerStub = { navigateByUrl: () => ({}), navigate: () => ({}) };
    const authServiceStub = { login: () => ({}), redirectUrl: {} };
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [LoginComponent],
      providers: [
        { provide: NgForm, useValue: ngFormStub },
        { provide: Router, useValue: routerStub },
        { provide: AuthService, useValue: authServiceStub }
      ]
    });
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  it('pageTitle defaults to: Log In', () => {
    expect(component.pageTitle).toEqual('Log In');
  });
  describe('login', () => {
    it('makes expected calls', () => {
      const routerStub: Router = fixture.debugElement.injector.get(Router);
      const authServiceStub: AuthService = fixture.debugElement.injector.get(
        AuthService
      );
      spyOn(routerStub, 'navigateByUrl');
      spyOn(routerStub, 'navigate');
      spyOn(authServiceStub, 'login');
      component.login(formValues);
      expect(routerStub.navigateByUrl).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalled();
      expect(authServiceStub.login).toHaveBeenCalled();
    });
  });
});
