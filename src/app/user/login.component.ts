import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent {
  errorMessage: string;
  pageTitle = 'Log In';
  loginData = {};

  constructor(private authService: AuthService,
              private router: Router) { }

  login(loginForm: NgForm) {
    console.log(loginForm.form);
    if (loginForm && loginForm.valid) {
      const userName = loginForm.form.value.userName;
      const password = loginForm.form.value.password;

    this.loginData = {
      userName : userName,
      password : password
    };

      this.authService.login(this.loginData);

      if (this.authService.redirectUrl) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.router.navigate(['/welcome']);
      }
    } else {
      this.errorMessage = 'Please enter a user name and password.';
    }
  }
}
